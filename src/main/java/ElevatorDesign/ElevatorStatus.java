package ElevatorDesign;

public class ElevatorStatus {
  public enum State {
    GOING_UP,
    GOING_DOWN,
    STOPPED,
    MAINTENANCE,
    BROKEN,
    IDLE
  };

  private int currentFloor;
  private State currentState;
  private int allowedWeight;

  public int getFloor() {
    return currentFloor;
  }

  public State getState() {
    return currentState;
  }

  public int getAllowedWeight(){return allowedWeight;}

  public ElevatorStatus(int floor, State state, int allowedWeight) {
    this.currentFloor = floor;
    this.currentState = state;
    this.allowedWeight = allowedWeight;
  }

  public void setFloor(int floor) {
    this.currentFloor = floor;
  }

  public void setState(State state) {
    this.currentState = state;
  }

  public void setAllowedWeight(int allowedWeight){this.allowedWeight = allowedWeight;}
}
