package ElevatorDesign;

public abstract class Elevator implements Runnable {
  public static ElevatorDesign.Elevator createElevator(int id, int minFloor, int maxFloor, int maxWeight, ElevatorDesign.ElevatorStatus initialStatus) {
    return new ElevatorDesign.ElevatorConcrete(id, maxWeight, new FloorChooserConcrete(minFloor, maxFloor, initialStatus));
  }

  public abstract ElevatorDesign.ElevatorStatus getStatus();

  public abstract void setMaintenance();

  public abstract void setBroken();

  public abstract void resetElevator();

  public abstract void gotoFloor(int floor);

  public abstract int getMaxWeight();

  public abstract int getId();

}
