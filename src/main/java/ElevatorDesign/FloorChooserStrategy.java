package ElevatorDesign;

public interface FloorChooserStrategy {
  public int getNextFloor();

  public void addFloor(int floor);

  public boolean hasMore();

  public void notify(ElevatorStatus status);
}
