package ElevatorDesign;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ElevatorController {
  private final ElevatorChooserStrategy _elevatorChooser;
  private final ArrayList<Elevator> _elevators;
  private static ElevatorDesign.ElevatorController INSTANCE;
  private static final Object _lockObj = new Object();
  private static ExecutorService _executor;

  private ElevatorController(ElevatorChooserStrategy elevatorChooser, ArrayList<Elevator> elevators) {
    _elevatorChooser = elevatorChooser;
    _elevators = elevators;
  }

  public static ElevatorDesign.ElevatorController getInstance(ElevatorChooserStrategy elevatorChooser, ArrayList<Elevator> elevators) {
    if (INSTANCE != null) return INSTANCE;

    synchronized (_lockObj) {
      if (INSTANCE == null) {
        INSTANCE = new ElevatorDesign.ElevatorController(elevatorChooser, elevators);
        _executor = Executors.newFixedThreadPool(elevators.size());
        for(Elevator e : elevators) {
          _executor.submit(e);
        }
      }
    }

    return INSTANCE;
  }

  public void callElevatorWeight(int floor, int incomingWeight) {
    Elevator bestElevator = _elevatorChooser.getBestElevator(floor, incomingWeight);
    callElevator(floor, bestElevator);
  }

  private void callElevator(int floor, Elevator elevator) {
    elevator.gotoFloor(floor);
  }

  public void callElevator(int floor, int elevator,int incomingWeight) {
    Elevator e = getElevator(elevator);
    if (e == null) return;
    int allowedWeight = e.getStatus().getAllowedWeight();
    if(allowedWeight >= incomingWeight){
      e.getStatus().setAllowedWeight(allowedWeight-incomingWeight);
      callElevator(floor,e);
    }
  }

  private Elevator getElevator(int elevator) {
    if (elevator < 0 || elevator >= _elevators.size()) return null;
    return _elevators.get(elevator);
  }

  public void setMaintenanceMode(int elevator) {
    Elevator e = getElevator(elevator);
    if (e == null) return;
    e.setMaintenance();
  }

  public void setBroken(int elevator) {
    Elevator e = getElevator(elevator);
    if (e == null) return;
    e.setBroken();
  }

  public void resetElevator(int elevator) {
    Elevator e = getElevator(elevator);
    if (e == null) return;
    e.resetElevator();
  }

  public void getState(int elevator){
    if (elevator < 0 || elevator >= _elevators.size()) System.out.println("Nothing found");
    System.out.println("Current State of Elevator: "+elevator + ":" + _elevators.get(elevator).getStatus().getState().name());
  }
}
