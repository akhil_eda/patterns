package ElevatorDesign;

import java.util.ArrayList;


public class ElevatorChooserConcrete implements ElevatorDesign.ElevatorChooserStrategy {
  private final ArrayList<Elevator> _elevators;
  private int currentWeight;

  public ElevatorChooserConcrete(ArrayList<Elevator> elevators) {
    _elevators = elevators;
  }

  @Override
  public Elevator getBestElevator(int floor, int incomingWeight) {
//    Check for Weight and
    Elevator bestElevator = _elevators.get(0);
    for (int i = 1; i < _elevators.size(); i++) {
      Elevator e = _elevators.get(i);
      int allowedWeight = e.getStatus().getAllowedWeight();
      int maxWeight = e.getMaxWeight();
      if(allowedWeight> maxWeight){
        e.getStatus().setAllowedWeight(maxWeight);
      }
      allowedWeight = e.getStatus().getAllowedWeight();
      if (e.getStatus().getState() != ElevatorStatus.State.BROKEN && e.getStatus().getState() != ElevatorStatus.State.MAINTENANCE
      && e.getStatus().getAllowedWeight() >= incomingWeight) {
        e.getStatus().setAllowedWeight(allowedWeight-incomingWeight);
        bestElevator = e;
      }
    }
    return bestElevator;
  }
}
