package ElevatorDesign;

public interface ElevatorChooserStrategy {
  public Elevator getBestElevator(int floor, int incomingWeight);
}
