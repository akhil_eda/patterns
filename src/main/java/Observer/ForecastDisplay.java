package Observer;

import java.util.Observable;
import java.util.Observer;

/**
 * created by Akhil.Eda on 02/05/20
 */
public class ForecastDisplay implements Observer, DisplayElement {
    Observable observable;
    private float currentPressure = 29.92f;
    private float lastPressure;

    public ForecastDisplay(Observable observable) {
        this.observable = observable;
        observable.addObserver(this);
    }

    public void update(Observable obs, Object arg) {
        if (obs instanceof WeatherData) {
            WeatherData weatherData = (WeatherData)obs;
            lastPressure = currentPressure;
            currentPressure = weatherData.getPressure();
            display();
        }
    }
    public void display() {
        System.out.println("Watch Out for cooler: "
                + "F degrees and "  + "% humidity");
    }

}
