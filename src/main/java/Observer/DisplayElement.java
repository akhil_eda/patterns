package Observer;

/**
 * created by Akhil.Eda on 02/05/20
 */
public interface DisplayElement {
    public void display();
}
