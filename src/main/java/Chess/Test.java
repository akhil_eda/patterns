package Chess;

/**
 * created by Akhil.Eda on 03/05/20
 */
public class Test {

    public static void test1() {

        Board b = new Board();
        b.createStandardPieceSet();
//        Moving knight of white
        System.out.println("Case1: ******************************");
        b.move(b.getSquare(7,1),b.getSquare(5,2));
        System.out.println("Moving White Knight to [5, 2] position ");

        String piece = b.getSquare(5,2).getPiece().getType();
        String color = b.getSquare(5,2).getPiece().getColorString();
        System.out.println(b.getBlackPieces());

        System.out.println("Possible moves for " + piece + " and color "+ color + b.getSquare(5,2).getPiece().getPossibleMoves());

        System.out.println("Case2: ******************************");

        b.move(b.getSquare(1,1),b.getSquare(3,1));
        System.out.println("Moving Black Pawn to [3, 1] position ");

        String piece1 = b.getSquare(3,1).getPiece().getType();
        String color1 = b.getSquare(3,1).getPiece().getColorString();
        System.out.println(b.getBlackPieces());

        System.out.println("Possible moves for " + piece1 + " and color "+ color1 + b.getSquare(3,1).getPiece().getPossibleMoves());


    }

    public static void main(String[] args) {
        test1();
    }

}