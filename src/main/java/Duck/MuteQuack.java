package Duck;

/**
 * created by Akhil.Eda on 01/05/20
 */
public class MuteQuack implements QuackBehaviour{
    public void quack() {
        System.out.println("<< Silence >>");
    }
}
