package Duck;

/**
 * created by Akhil.Eda on 01/05/20
 */
public interface FlyBehaviour {

    public void fly();

}
