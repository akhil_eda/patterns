package Duck;

/**
 * created by Akhil.Eda on 01/05/20
 */
public class MallardDuck extends Duck {
    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }
    public void display() {
        System.out.println("I’m a real Mallard duck");
    }
}
