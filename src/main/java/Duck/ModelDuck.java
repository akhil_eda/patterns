package Duck;

/**
 * created by Akhil.Eda on 01/05/20
 */
public class ModelDuck extends Duck{
    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }
    public void display() {
        System.out.println("I’m a model duck");
    }
}
