package Duck;

/**
 * created by Akhil.Eda on 01/05/20
 */
public class MiniDuckSimulator {
    public static void main(String[] args) {
//        Duck mallard = new MallardDuck();
//        mallard.performQuack();
//        mallard.performFly();
//        mallard.display();
//        System.out.println("Mallard Duck case is done");

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}
