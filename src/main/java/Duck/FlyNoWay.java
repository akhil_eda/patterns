package Duck;

/**
 * created by Akhil.Eda on 01/05/20
 */
public class FlyNoWay implements FlyBehaviour {
    public void fly() {
        System.out.println("I can’t fly");
    }
}
