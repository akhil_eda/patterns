package Duck;

/**
 * created by Akhil.Eda on 01/05/20
 */
public class Quack implements QuackBehaviour {
    public void quack() {
        System.out.println("Quack");
    }
}
