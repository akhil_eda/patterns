package Duck;

/**
 * created by Akhil.Eda on 01/05/20
 */
public interface QuackBehaviour {
    public void quack();
}
